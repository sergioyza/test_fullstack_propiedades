# Examen Práctico: Software Engineer


![alt text](https://i.ibb.co/S0LGhCV/screencapture-127-0-0-1-8000-inmuebles-2022-01-05-15-06-05.png)


![alt text](https://i.ibb.co/qghYbtg/screencapture-127-0-0-1-8000-inmuebles-2-2022-01-05-15-07-02.png)

Para la realización de este proyecto se atiendieron las siguientes instrucciones

La inmobiliaria **OKOL** requiere de una aplicación web para la administración de sus inmuebles con las siguientes consideraciones:


-  Permitir el registro y  autenticación de usuarios **COMPLETADO**
-  El sistema deberá permitir la creación, lectura, borrado y edición de inmuebles  **COMPLETADO**
-  La creación de inmuebles debe considerar los siguientes criterios:
    - El formulario  de  registro  para los inmuebles debe contar con las siguientes restricciones:
        - Nombre del inmueble: validar solo el uso de textos **COMPLETADO**
        - Clave del inmueble: respetar el siguiente formato **PCOM-XXX/##** **COMPLETADO**
        - Galeria de imagenes: deberá consumir el **API de Instagram** para obtener 5 fotos de localidades cercanas a la ubicación del inmueble y almacenarlas en base de datos como parte de su galería **INCOMPLETADO**
            -   Para este punto se constaron las siguientes apis sin embargo no se concluyo su integracion:
                - https://api.instagram.com/oauth/authorize?client_id=506956073944073&redirect_uri=https://www.google.com/&scope=user_profile,user_media&response_type=code obtiene el code.
                - https://api.instagram.com/oauth/access_token metodo post para genera token
                - https://graph.instagram.com/me/media?fields=id,caption&access_token=IGQ... obtener info de las imagenes del mismo token.
                - https://graph.instagram.com/17991933310142679?fields=id,media_type,media_url,username,timestamp&access_token=IGQV... Se obtiene la imagenes encontradas.
                - https://api.instagram.com/v1/media/search?lat=48.858844&lng=2.294351&access_token=IGQV... Ruta no permite obtener ids de imagenes rebota un 400.
        - Descripción, tipo de transacción (venta / renta), tipo de inmueble (casa, departamento, terreno, etc) y el precio del inmueble **COMPLETADO**
    - Datos de ubicación mínimos requeridos: Estado, Delegación o municipio, Colonia, calle y número **COMPLETADO**
    - Datos de geoposicionamiento: latitud y longitud **COMPLETADO**
    - Características: Se puede agregar N características al inmueble (Ejemplo: cisterna, área de lavado, seguridad privada, etc) **COMPLETADO**
- El sistema deberá permitir la edición de todos los campos antes mencionados a excepción de la clave del inmueble **COMPLETADO**
    - Esta funcionalidad solo esta permitida para usuarios logeados **COMPLETADO**
- Deberá existir una sección donde se muestre un mapa con el **API de google maps** donde se visualicen los pins de los inmuebles registrados **COMPLETADO**
    - La posicion del pin para cada inmueble en el mapa corresponde a su latutud y longitud registrada **COMPLETADO**
    - Al seleccionar   cualquier   pin   del   mapa; debe mostrar la ficha de detalle del inmueble
- Deberá existir una sección donde se listaran los inmuebles registrados como se muestra en la siguiente imagen **COMPLETADO**
    - El listado se podra ver para todo tipo de usuario **COMPLETADO**
    - Los botones de edición y borrado solo se deberan mostrar para usuarios logeados **COMPLETADO**
    - Cada inmueble del listado, debera mostrar sobre la imagen clave del inmueble, calle y numero **COMPLETADO**

![alt text](https://propiedadescom.s3.amazonaws.com/examen/examen.png)

- Al dar click en un inmueble del listado, se debera abrir la ficha tecnica del inmueble como se miestra en la siguiente imagen

![alt text](https://propiedadescom.s3.amazonaws.com/examen/examen2.png)

# Importante:
    - El diseño se debe apegar a las imagenes proporcionadas
    - La aplicación debera ser responsiva
----------


Requerimientos:
-------------

- Configura un servidor local de tu preferencia para verificar el funcionamiento del ejercicio.
- Repositorio:
    - Debe contar con una cuenta en **bitbucket** para poder bifurcar (**fork**) el repositorio
    - Realice el examen práctico  sobre la copia que realizó
    - Al finalizar la prueba, enviar el link de su repositorio al siguiente correo: **luis.castro@propiedades.com**

Especificaciones:
-------------
- Tiene la libertad de modelar la estructura de la **BD** de acuerdo a sus necesidades.
- El uso de los commit debe contar con una descripción detallada.
- Al finalizar la prueba, en la carpeta **BD** crear el archivo **dump.sql** de la estructura final de la base de datos que requiere
- Estructura del repositorio:

bd
:  query.sql

web
:  index.php

**config.txt**

-  En el archivo **config.txt**  especificar  la configuración que su examen requiere para que pueda funcionar correctamente.  Si realizo el uso de algún **framework**, favor de especificar el nombre y la versión  para su adecuado funcionamiento.
