-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.33 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para propiedades
CREATE DATABASE IF NOT EXISTS `propiedades` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci */;
USE `propiedades`;

-- Volcando estructura para tabla propiedades.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla propiedades.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla propiedades.inmuebles
CREATE TABLE IF NOT EXISTS `inmuebles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_tipo_trans` enum('venta','renta') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'renta',
  `d_tipo_inmu` enum('casa','departamento','terreno','otros') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'casa',
  `price` double(15,2) NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inmuebles_user_id_foreign` (`user_id`),
  CONSTRAINT `inmuebles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla propiedades.inmuebles: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `inmuebles` DISABLE KEYS */;
INSERT INTO `inmuebles` (`id`, `name`, `description`, `key`, `d_tipo_trans`, `d_tipo_inmu`, `price`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 'Bonita casa con todos los servicios', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at sollicitudin lacus. Phasellus scelerisque tincidunt tempus. Cras mollis accumsan rhoncus. Sed sed nisi maximus, dapibus massa quis, semper felis. In in mattis erat. Curabitur efficitur, nisl in aliquet laoreet, purus nisi viverra sapien, quis fermentum diam diam at risus. Donec non sem nunc', 'PCOM-XXX/1', 'venta', 'casa', 4000.00, 1, '2022-01-05 19:40:31', '2022-01-05 19:48:48'),
	(2, 'Vendo casa en la colonia San Lucas CDMX', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at sollicitudin lacus. Phasellus scelerisque tincidunt tempus. Cras mollis accumsan rhoncus. Sed sed nisi maximus, dapibus massa quis, semper felis. In in mattis erat. Curabitur efficitur, nisl in aliquet laoreet, purus nisi viverra sapien, quis fermentum diam diam at risus. Donec non sem nunc.', 'PCOM-XXX/2', 'venta', 'casa', 4000000.00, 1, '2022-01-05 19:45:39', '2022-01-05 19:49:01'),
	(3, 'Lo mejor de la calle San Francisco', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at sollicitudin lacus. Phasellus scelerisque tincidunt tempus. Cras mollis accumsan rhoncus. Sed sed nisi maximus, dapibus massa quis, semper felis. In in mattis erat. Curabitur efficitur, nisl in aliquet laoreet, purus nisi viverra sapien, quis fermentum diam diam at risus. Donec non sem nunc.', 'PCOM-XXX/3', 'renta', 'casa', 3000.00, 1, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(4, 'Cinco de Mayo te espera', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at sollicitudin lacus. Phasellus scelerisque tincidunt tempus. Cras mollis accumsan rhoncus. Sed sed nisi maximus, dapibus massa quis, semper felis. In in mattis erat. Curabitur efficitur, nisl in aliquet laoreet, purus nisi viverra sapien, quis fermentum diam diam at risus. Donec non sem nunc.', 'PCOM-XXX/4', 'renta', 'departamento', 8400.00, 1, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(5, 'Ayudanos a ayudarte desde canal', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at sollicitudin lacus. Phasellus scelerisque tincidunt tempus. Cras mollis accumsan rhoncus. Sed sed nisi maximus, dapibus massa quis, semper felis. In in mattis erat. Curabitur efficitur, nisl in aliquet laoreet, purus nisi viverra sapien, quis fermentum diam diam at risus. Donec non sem nunc.', 'PCOM-XXX/5', 'renta', 'terreno', 15000.00, 1, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(6, 'Se renta departamento de lujo departamento en CDMX', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at sollicitudin lacus. Phasellus scelerisque tincidunt tempus. Cras mollis accumsan rhoncus. Sed sed nisi maximus, dapibus massa quis, semper felis. In in mattis erat. Curabitur efficitur, nisl in aliquet laoreet, purus nisi viverra sapien, quis fermentum diam diam at risus. Donec non sem nunc.', 'PCOM-XXX/6', 'renta', 'departamento', 12000.00, 1, '2022-01-05 20:00:22', '2022-01-05 20:00:22');
/*!40000 ALTER TABLE `inmuebles` ENABLE KEYS */;

-- Volcando estructura para tabla propiedades.inmuebles_caractes
CREATE TABLE IF NOT EXISTS `inmuebles_caractes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `caracteristicas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inmueble_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inmuebles_caractes_inmueble_id_foreign` (`inmueble_id`),
  CONSTRAINT `inmuebles_caractes_inmueble_id_foreign` FOREIGN KEY (`inmueble_id`) REFERENCES `inmuebles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla propiedades.inmuebles_caractes: ~20 rows (aproximadamente)
/*!40000 ALTER TABLE `inmuebles_caractes` DISABLE KEYS */;
INSERT INTO `inmuebles_caractes` (`id`, `caracteristicas`, `inmueble_id`, `created_at`, `updated_at`) VALUES
	(13, 'clima', 1, '2022-01-05 19:48:48', '2022-01-05 19:48:48'),
	(14, 'estacionamiento', 1, '2022-01-05 19:48:48', '2022-01-05 19:48:48'),
	(15, 'patio', 1, '2022-01-05 19:48:48', '2022-01-05 19:48:48'),
	(16, 'Cafetera', 2, '2022-01-05 19:49:01', '2022-01-05 19:49:01'),
	(17, 'Cocina', 2, '2022-01-05 19:49:01', '2022-01-05 19:49:01'),
	(18, 'Lavadora', 2, '2022-01-05 19:49:01', '2022-01-05 19:49:01'),
	(19, 'Microondas', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(20, 'Cafetera', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(21, 'Jacuzzi', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(22, 'Cocina', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(23, 'Lavadora', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(24, 'Wifi', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(25, 'Refrigerador', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(26, 'Microondas', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(27, 'Jacuzzi', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(28, 'estacionamiento', 5, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(29, 'patio', 5, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(45, 'TV', 6, '2022-01-05 20:02:07', '2022-01-05 20:02:07'),
	(46, 'Botiquín', 6, '2022-01-05 20:02:07', '2022-01-05 20:02:07'),
	(47, 'Wifi', 6, '2022-01-05 20:02:07', '2022-01-05 20:02:07');
/*!40000 ALTER TABLE `inmuebles_caractes` ENABLE KEYS */;

-- Volcando estructura para tabla propiedades.inmuebles_imagenes
CREATE TABLE IF NOT EXISTS `inmuebles_imagenes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubicacion_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inmuebles_imagenes_ubicacion_id_foreign` (`ubicacion_id`),
  CONSTRAINT `inmuebles_imagenes_ubicacion_id_foreign` FOREIGN KEY (`ubicacion_id`) REFERENCES `inmuebles_ubicaciones` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla propiedades.inmuebles_imagenes: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `inmuebles_imagenes` DISABLE KEYS */;
INSERT INTO `inmuebles_imagenes` (`id`, `url`, `ubicacion_id`, `created_at`, `updated_at`) VALUES
	(1, 'https://picsum.photos/200/200?random=303320', 1, '2022-01-05 19:40:31', '2022-01-05 19:40:31'),
	(2, 'https://picsum.photos/200/200?random=162199', 1, '2022-01-05 19:40:31', '2022-01-05 19:40:31'),
	(3, 'https://picsum.photos/200/200?random=819554', 1, '2022-01-05 19:40:31', '2022-01-05 19:40:31'),
	(4, 'https://picsum.photos/200/200?random=226883', 1, '2022-01-05 19:40:31', '2022-01-05 19:40:31'),
	(5, 'https://picsum.photos/200/200?random=700562', 1, '2022-01-05 19:40:31', '2022-01-05 19:40:31'),
	(6, 'https://picsum.photos/200/200?random=494058', 1, '2022-01-05 19:40:31', '2022-01-05 19:40:31'),
	(7, 'https://picsum.photos/200/200?random=476407', 2, '2022-01-05 19:45:39', '2022-01-05 19:45:39'),
	(8, 'https://picsum.photos/200/200?random=549492', 2, '2022-01-05 19:45:39', '2022-01-05 19:45:39'),
	(9, 'https://picsum.photos/200/200?random=656916', 2, '2022-01-05 19:45:39', '2022-01-05 19:45:39'),
	(10, 'https://picsum.photos/200/200?random=292138', 2, '2022-01-05 19:45:39', '2022-01-05 19:45:39'),
	(11, 'https://picsum.photos/200/200?random=296487', 2, '2022-01-05 19:45:39', '2022-01-05 19:45:39'),
	(12, 'https://picsum.photos/200/200?random=681886', 2, '2022-01-05 19:45:39', '2022-01-05 19:45:39'),
	(13, 'https://picsum.photos/200/200?random=653143', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(14, 'https://picsum.photos/200/200?random=463012', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(15, 'https://picsum.photos/200/200?random=949977', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(16, 'https://picsum.photos/200/200?random=968787', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(17, 'https://picsum.photos/200/200?random=635683', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(18, 'https://picsum.photos/200/200?random=288079', 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(19, 'https://picsum.photos/200/200?random=785625', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(20, 'https://picsum.photos/200/200?random=851112', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(21, 'https://picsum.photos/200/200?random=693256', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(22, 'https://picsum.photos/200/200?random=129117', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(23, 'https://picsum.photos/200/200?random=151699', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(24, 'https://picsum.photos/200/200?random=218842', 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(25, 'https://picsum.photos/200/200?random=621840', 5, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(26, 'https://picsum.photos/200/200?random=282770', 5, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(27, 'https://picsum.photos/200/200?random=739618', 5, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(28, 'https://picsum.photos/200/200?random=329534', 5, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(29, 'https://picsum.photos/200/200?random=901789', 5, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(30, 'https://picsum.photos/200/200?random=580756', 5, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(31, 'https://picsum.photos/200/200?random=784179', 6, '2022-01-05 20:00:22', '2022-01-05 20:00:22'),
	(32, 'https://picsum.photos/200/200?random=100715', 6, '2022-01-05 20:00:22', '2022-01-05 20:00:22'),
	(33, 'https://picsum.photos/200/200?random=651024', 6, '2022-01-05 20:00:22', '2022-01-05 20:00:22'),
	(34, 'https://picsum.photos/200/200?random=615153', 6, '2022-01-05 20:00:22', '2022-01-05 20:00:22'),
	(35, 'https://picsum.photos/200/200?random=555683', 6, '2022-01-05 20:00:22', '2022-01-05 20:00:22'),
	(36, 'https://picsum.photos/200/200?random=723911', 6, '2022-01-05 20:00:22', '2022-01-05 20:00:22');
/*!40000 ALTER TABLE `inmuebles_imagenes` ENABLE KEYS */;

-- Volcando estructura para tabla propiedades.inmuebles_ubicaciones
CREATE TABLE IF NOT EXISTS `inmuebles_ubicaciones` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `municipio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colonia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `inmueble_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inmuebles_ubicaciones_inmueble_id_foreign` (`inmueble_id`),
  CONSTRAINT `inmuebles_ubicaciones_inmueble_id_foreign` FOREIGN KEY (`inmueble_id`) REFERENCES `inmuebles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla propiedades.inmuebles_ubicaciones: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `inmuebles_ubicaciones` DISABLE KEYS */;
INSERT INTO `inmuebles_ubicaciones` (`id`, `estado`, `municipio`, `colonia`, `calle`, `numero`, `lat`, `lng`, `inmueble_id`, `created_at`, `updated_at`) VALUES
	(1, 'CDMX', 'Doctores', 'San lucas', 'LosReyes', '103', -99.1060202, 19.358549, 1, '2022-01-05 19:40:31', '2022-01-05 19:40:31'),
	(2, 'CDMX', 'San Juan', 'San miguel', 'Xoxotla, San Pablo', '101', -99.087544, 19.3601932, 2, '2022-01-05 19:45:39', '2022-01-05 19:47:03'),
	(3, 'CDMX', 'Cuahutemoc', 'Mujeres', 'San francisco', '76', -99.171879, 19.3883766, 3, '2022-01-05 19:53:28', '2022-01-05 19:53:28'),
	(4, 'CDMX', 'Tlalpan', 'San lucas', '5 de Mayo, San Lucas', '25', -99.0983816, 19.3624971, 4, '2022-01-05 19:55:40', '2022-01-05 19:55:40'),
	(5, 'CDMX', 'Tlalpan', 'Leyes', 'Batalla de Loma Alta', '99', -99.069384, 19.3791698, 5, '2022-01-05 19:57:41', '2022-01-05 19:57:41'),
	(6, 'CDMX', 'Tlalpan', 'Constitucion', 'Ramon Ross', '50', -99.0639914, 19.3514411, 6, '2022-01-05 20:00:22', '2022-01-05 20:00:22');
/*!40000 ALTER TABLE `inmuebles_ubicaciones` ENABLE KEYS */;

-- Volcando estructura para tabla propiedades.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla propiedades.migrations: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(5, '2022_01_03_191922_create_inmuebles_table', 1),
	(6, '2022_01_03_193651_create_inmuebles_caractes_table', 1),
	(7, '2022_01_03_194422_create_inmuebles_ubicaciones_table', 1),
	(8, '2022_01_03_195352_create_inmuebles_imagenes_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla propiedades.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla propiedades.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla propiedades.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla propiedades.personal_access_tokens: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;

-- Volcando estructura para tabla propiedades.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla propiedades.users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Sergio pucheta', 'sergioyza@gmail.com', NULL, '$2y$10$ll2RoW2VbnbkMMhmbqie8e2D6M2sPLxgT2oUGF.jl0qnQASg8wuei', NULL, '2022-01-05 19:35:39', '2022-01-05 19:35:39');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
