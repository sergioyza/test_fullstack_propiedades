<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inmueble extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'key',
        'd_tipo_trans',
        'd_tipo_inmu',
    ];

    public function caracteristicas()
    {
        return $this->hasMany('App\Models\InmueblesCaracte');
    }
    public function ubicacion()
    {
        return $this->hasOne('App\Models\InmueblesUbicacione');
    }
}
