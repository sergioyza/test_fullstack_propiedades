<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmueblesUbicacione extends Model
{
    use HasFactory;

    public function imagenes()
    {
      return $this->hasMany('App\Models\InmueblesImagene', 'ubicacion_id', 'id');
    }

}
