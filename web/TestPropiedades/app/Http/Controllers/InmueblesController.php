<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Auth;
use App\Models\Inmueble;
use App\Models\InmueblesCaracte;
use App\Models\InmueblesIMagene;
use App\Models\InmueblesUbicacione;

class InmueblesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inmuebles=Inmueble::all();
        return view('Inmueble.index',compact('inmuebles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Inmueble.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $this->validate($request,[
        'nombre'=>'required|regex:/^[\pL\s\-]+$/u|max:255|min:10',
        'tipo_trans'=>'required',
        'tipo_inmueble'=>'required',
        'precio'=>'required|Numeric',
        'description'=>'required|max:1500|min:10',
        'estado'=>'required|regex:/^[\pL\s\-]+$/u|max:255|min:3',
        'municipio'=>'required|regex:/^[\pL\s\-]+$/u|max:255|min:3',
        'colonia'=>'required|regex:/^[\pL\s\-]+$/u|max:255|min:3',
        'calle'=>'required|max:255|min:5',
        'numeroDireccion'=>'required|alpha_num|max:15|min:1',
        'lat'=>'required',
        'lng'=>'required']);
      $inmueble = new Inmueble;
      $inmueble->name = $request->nombre;

      $inmueble->d_tipo_trans = $request->tipo_trans;
      $inmueble->d_tipo_inmu = $request->tipo_inmueble;
      $inmueble->price = $request->precio;
      $inmueble->key = 'PCOM-XXX/';
      $inmueble->description = $request->description;
      $inmueble->user_id = Auth::id();
      $inmueble->save();
      $inmueble->key = 'PCOM-XXX/'.$inmueble->id;
      $inmueble->save();

      $caracteristicas = $request->caracteristicas;
      foreach($caracteristicas as $caracteristica){

        $inmueblesCaracte = new InmueblesCaracte;
        $inmueblesCaracte->inmueble_id = $inmueble->id;
        $inmueblesCaracte->caracteristicas = $caracteristica;
        $inmueblesCaracte->save();
       }


      $inmueblesUbicacione = new InmueblesUbicacione;
      $inmueblesUbicacione->estado = $request->estado;
      $inmueblesUbicacione->municipio = $request->municipio;
      $inmueblesUbicacione->colonia = $request->colonia;
      $inmueblesUbicacione->calle = $request->calle;
      $inmueblesUbicacione->numero = $request->numeroDireccion;
      $inmueblesUbicacione->lat = $request->lat;
      $inmueblesUbicacione->lng = $request->lng;
      $inmueblesUbicacione->inmueble_id =  $inmueble->id;
      $inmueblesUbicacione->save();

      for ($i = 1; $i <= 6; $i++) {
        $inmueblesIMagene = new InmueblesIMagene;
        $inmueblesIMagene->ubicacion_id= $inmueblesUbicacione->id;
        $inmueblesIMagene->url = 'https://picsum.photos/200/200?random='.rand(1,1000000);
        $inmueblesIMagene->save();
      }


      return redirect()->route('inmuebles.index')->with('success','Registro Creado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $inmueble=Inmueble::find($id);
      return  view('Inmueble.show',compact('inmueble'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $inmueble=Inmueble::find($id);

      return view('Inmueble.edit',compact('inmueble'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'nombre'=>'required|regex:/^[\pL\s\-]+$/u|max:255|min:10',
        'tipo_trans'=>'required',
        'tipo_inmueble'=>'required',
        'precio'=>'required|Numeric',
        'description'=>'required|max:1500|min:10',
        'estado'=>'required|regex:/^[\pL\s\-]+$/u|max:255|min:3',
        'municipio'=>'required|regex:/^[\pL\s\-]+$/u|max:255|min:3',
        'colonia'=>'required|regex:/^[\pL\s\-]+$/u|max:255|min:3',
        'calle'=>'required|max:255|min:5|',
        'numeroDireccion'=>'required|alpha_num|max:15|min:1',
        'lat'=>'required',
        'lng'=>'required']);

        $inmueble =  Inmueble::find($id);
        $inmueble->name = $request->nombre;

        $inmueble->d_tipo_trans = $request->tipo_trans;
        $inmueble->d_tipo_inmu = $request->tipo_inmueble;
        $inmueble->price = $request->precio;
        $inmueble->key = 'PCOM-XXX/'.$inmueble->id;
        $inmueble->description = $request->description;
        $inmueble->user_id = Auth::id();
        $inmueble->save();

        $inmueblesUbicacione = InmueblesUbicacione::find($inmueble->id);
        $inmueblesUbicacione->estado = $request->estado;
        $inmueblesUbicacione->municipio = $request->municipio;
        $inmueblesUbicacione->colonia = $request->colonia;
        $inmueblesUbicacione->calle = $request->calle;
        $inmueblesUbicacione->numero = $request->numeroDireccion;
        $inmueblesUbicacione->lat = $request->lat;
        $inmueblesUbicacione->lng = $request->lng;
        $inmueblesUbicacione->inmueble_id =  $inmueble->id;
        $inmueblesUbicacione->save();

        $model=InmueblesCaracte::where('inmueble_id',$inmueble->id)->delete();

        $caracteristicas = $request->caracteristicas;
        foreach($caracteristicas as $caracteristica){

          $inmueblesCaracte = new InmueblesCaracte;
          $inmueblesCaracte->inmueble_id = $inmueble->id;
          $inmueblesCaracte->caracteristicas = $caracteristica;
          $inmueblesCaracte->save();
         }

        return redirect()->route('inmuebles.index')->with('success','Registro modificado satisfactoriamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Inmueble::find($id)->delete();
      return redirect()->route('inmuebles.index')->with('success','Registro eliminado satisfactoriamente');
    }
}
