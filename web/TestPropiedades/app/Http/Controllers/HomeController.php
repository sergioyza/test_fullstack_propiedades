<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inmueble;
use App\Models\InmueblesCaracte;
use App\Models\InmueblesIMagene;
use App\Models\InmueblesUbicacione;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$inmueble = Inmueble::find(1);

        //$inmueblecaracs = Inmueble::find(1)->caracteristicas;
        //$inmuebleimgs = Inmueble::find(1)->ubicacion->imagenes;
        //$inmuebleubi = Inmueble::find(1)->ubicacion;

        //$carac = InmueblesCaracte::find(1);
        //$img = InmueblesIMagene::find(1);
        //$ubi = InmueblesUbicacione::find(1);
        //return view('Inmueble.index')->with('title','hola mundo');
        return redirect()->route('inmuebles.index');
    }
}
