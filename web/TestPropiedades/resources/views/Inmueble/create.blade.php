@extends('layouts.app')

@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger">
	<strong>Error!</strong> Revise los campos obligatorios.<br><br>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
@if(Session::has('success'))
<div class="alert alert-info">
	{{Session::get('success')}}
</div>
@endif
<div class="container text-right">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Creación de nuevo inmueble</div>
                <div class="card-body">
                  <form method="POST" action="{{ route('inmuebles.store') }}"  role="form">
							             {{ csrf_field() }}
                    <div class="form-group row mb-2">
                      <label for="inputNombre" class="col-md-2 col-form-label">Nombre</label>
                      <div class="col-md-10">
                        <input type="input" class="form-control" id="inputNombre" placeholder="Nombre" name="nombre">
                      </div>
                    </div>
                    <div class="form-group row mb-2">
                      <label for="inputNombre" class="col-md-2 col-form-label">Descripción</label>
                      <div class="col-md-10">
                        <textarea class="form-control" rows="5" id="description" name="description"></textarea>
                      </div>
                    </div>
                    <div class="form-group row mb-2">
                      <label for="select" class="col-md-2 col-form-label">Tipo de transacción</label>
                      <div class="col-md-2">
                        <select class="form-select" aria-label="Default select example" name="tipo_trans">
                          <option value="venta">Venta</option>
                          <option value="renta">Renta</option>
                        </select>
                      </div>
                      <label for="select" class="col-md-2 col-form-label">Tipo de inmueble</label>
                      <div class="col-md-2">
                        <select class="form-select" aria-label="Default select example" name="tipo_inmueble">
                          <option value="casa">Casa</option>
                          <option value="departamento">Departamento</option>
                          <option value="terreno">Terreno</option>
                          <option value="otros">Otros</option>
                        </select>
                      </div>
                      <label for="select" class="col-md-2 col-form-label">Precio en MXN</label>
                      <div class="col-md-2">
                          <input type="input" class="form-control" id="inputPrecio" placeholder="Precio"  name="precio">
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label for="inputNombre" class="col-md-2 col-form-label">caracteristicas</label>
                      <div class="col-md-10 text-left">
												<select class="form-select js-example-basic-multiple" name="caracteristicas[]" multiple="multiple">
													<option value="clima">Clima</option>
													<option value="estacionamiento" >Estacionamiento</option>
													<option value="patio">Patio</option>
													<option value="Shampoo">Shampoo</option>
													<option value="Agua caliente">Agua caliente</option>
													<option value="Ganchos">Ganchos</option>
													<option value="Ropa de cama">Ropa de cama</option>
													<option value="TV">TV</option>
													<option value="Calefacción">Calefacción</option>
													<option value="Botiquín">Botiquín</option>
													<option value="Wifi">Wifi</option>
													<option value="Refrigerador">Refrigerador</option>
													<option value="Microondas">Microondas</option>
													<option value="Cafetera">Cafetera</option>
													<option value="Barbacoa">Barbacoa</option>
													<option value="Jacuzzi">Jacuzzi</option>
													<option value="Cocina">Cocina</option>
													<option value="Lavadora">Lavadora</option>
												</select>
                      </div>
                    </div>
										<div class="form-group row mb-2">
                      <label for="inputNombre" class="col-md-2 col-form-label">Estado</label>
                      <div class="col-md-2">
                        <input type="input" class="form-control" id="inputEstado" placeholder="Estado" name="estado">
                      </div>
                      <label for="inputNombre" class="col-md-2 col-form-label">Delegación o municipio</label>
                      <div class="col-md-2">
                        <input type="input" class="form-control" id="inputEstado" placeholder="Delegación o municipio" name="municipio">
                      </div>
                      <label for="inputNombre" class="col-md-2 col-form-label">Colonia</label>
                      <div class="col-md-2">
                        <input type="input" class="form-control" id="inputColonia" placeholder="Colonia" name="colonia">
                      </div>
                    </div>
										<div class="form-group row mb-2">
                      <label for="inputNombre" class="col-md-2 col-form-label">Calle</label>
                      <div class="col-md-6">
                        <input type="input" class="form-control" id="inputCalle" placeholder="Calle" name="calle">
                      </div>
                      <label for="inputNombre" class="col-md-2 col-form-label">Numero</label>
                      <div class="col-md-2">
                        <input type="input" class="form-control" id="inputNumero" placeholder="Numero" name="numeroDireccion">
                      </div>
                    </div>

										<div class="form-group">
										    <label for="address_address">Dirección</label>
										    <input type="text" id="address-input" name="address_address" class="form-control map-input">
										    <input type="hidden" name="lng" id="address-latitude" />
										    <input type="hidden" name="lat" id="address-longitude" />
										</div>
										<div id="address-map-container" style="width:100%;height:400px; ">
										    <div style="width: 100%; height: 100%" id="address-map"></div>
										</div>
										<div class="form-group row mb-2">
                      <input type="submit"  value="Guardar" class="btn btn-info col-md-3">
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
@section('scripts')
    @parent
		<script src="/js/mapInput.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>
		<script type="text/javascript">
		$(document).ready(function() {
		    $('.js-example-basic-multiple').select2();
		});
		</script>
@stop
