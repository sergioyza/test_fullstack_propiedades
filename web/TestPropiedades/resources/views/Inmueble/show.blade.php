@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

									<div class="container">
                    <div class="card mb-2">
                      <div class="card-header mb-2">
                        <div class="row">
                          <div class="col-md-10">
                          Información del Inmueble
                          </div>

                          @auth
                          <div class="col-md-1">
                            <a style="color:gray" href="{{ url("/inmuebles/{$inmueble->id}/edit") }}" ><i class="fas fa-pen fa-2x"> </i></a>

                          </div>
                          <div class="col-md-1">
                            <form method="POST" action="{{ route('inmuebles.destroy',$inmueble->id) }}"  role="form">
                                   {{csrf_field()}}
                                   <input name="_method" type="hidden" value="DELETE">
                                   <button class="btn btn-primary-outline" style="color:gray" type="submit"><i class="fas fa-trash-alt fa-2x"></i></button>
                            </form>
                          </div>

                          @endauth
                        </div>

                      </div>
                      <div class="col-md-12">
                        <img style="width: 100%;object-fit: cover;height: 400px; " id="main-imagen" src="{{$inmueble->ubicacion->imagenes->first()->url}}" class="rounded img-responsive" alt="...">
                      </div>

                      <div class="centrado">
                        <p class="card-text">Tipo de propiedad: <b>{{ $inmueble->d_tipo_inmu}}</b>, Tipo de transacción <b>{{ $inmueble->d_tipo_trans}}</b></p>
                        <h5 class="card-title">Nombre del titulo: {{ $inmueble->name}}</h5>
                        <p class="card-text">Estado: {{$inmueble->ubicacion->estado}}, Colonia: {{$inmueble->ubicacion->colonia}}, Calle: {{$inmueble->ubicacion->calle}}, Numero: {{$inmueble->ubicacion->numero}}</p>
                        <h5 class="card-title">Disponible desde ${{ $inmueble->price}} </h5>
                      </div>
                      <div class="card-body">

                        <div id="carousel" class="carousel slide mb-4 mt-2" data-ride="carousel">
                          <div class="carousel-inner">
                            @php
                            $count = 0;
                            $flag=0
                            @endphp

                                @foreach ($inmueble->ubicacion->imagenes as $imagen)
                                @php
                                $flag = $flag+1;
                                @endphp

                                @if($count==0 or  $count%3==0)
                                @if($count==0)
                                <div class="carousel-item active">
                                  <div class="row">
                                @else
                                <div class="carousel-item">
                                  <div class="row">
                                @endif

                                @endif


                                <div class="col-md-4">
                                  <img style="width: 85%;object-fit: cover;height: 85%; " id="img-1" src="{{$imagen->url}}" class="rounded img-fluid img-responsive" alt="..."  width = "100%" onClick="reply_click(this.src)">
                                </div>
                                @if($flag==3 )
                                  </div>
                                </div>
                                @php
                                $flag=0
                                @endphp
                                @endif
                                @php
                                $count = $count+1;
                                @endphp
                                @endforeach

                          <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                      </div>
                    </div>

										<div class="card">
										  <div class="card-body">
										    <h5 class="card-title">Detalles del inmueble</h5>
										    <p class="card-text">{{ $inmueble->description}}</p>
												<h5 class="card-title">Caracteristicas del inmueble</h5>
												<ul class="list-group list-group-horizontal">
													@foreach($inmueble->caracteristicas as $caract)
												  <li class="list-group-item">{{$caract->caracteristicas}}</li>
													@endforeach
												</ul>
										  </div>
										</div>
									</div>
                  <div class="form-group">

                      <input type="hidden" name="lng" id="address-latitude"/>
                      <input type="hidden" name="lat" id="address-longitude"/>
                  </div>
                  <div id="address-map-container" style="width:100%;height:400px; ">
                      <div style="width: 100%; height: 100%" id="address-map"></div>
                  </div>
    </div>
</div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
		function initialize() {

		    const latitude = 19.3545705;
		    const longitude = -99.084642;
				 var data = {!! json_encode($inmueble, JSON_HEX_TAG) !!};
				 console.log(data.length)

		    const map = new google.maps.Map(document.getElementById('address-map'), {
		        center: {lat: data.ubicacion.lng, lng: data.ubicacion.lat},
		        zoom: 13
		    });

				  console.log(data.ubicacion.lat)
					console.log(data.ubicacion.lng)

					const marker = new google.maps.Marker({
			        map: map,
			        position: {lat: data.ubicacion.lng, lng: data.ubicacion.lat},
			    });

			    marker.setVisible(true);
				}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>
    <script type="text/javascript">
//así con JS normal
//accediendo directamente a src

      function reply_click(clicked_id)
      {
          document.getElementById("main-imagen").src=clicked_id;
      }
    </script>
@stop
