@extends('layouts.app')

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
	<strong>Error!</strong> Revise los campos obligatorios.<br><br>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
@if(Session::has('success'))
<div class="alert alert-info">
	{{Session::get('success')}}
</div>
@endif
<div class="container">
  <div class="form-group">

      <input type="hidden" name="lng" id="address-latitude"/>
      <input type="hidden" name="lat" id="address-longitude"/>
  </div>
  <div id="address-map-container" style="width:100%;height:400px; ">
      <div style="width: 100%; height: 100%" id="address-map"></div>
  </div>
  <div class="row">
    @foreach($inmuebles as $inmueble)
		<div class="col-md-4">
			<div class="card p-4 m-2" >
				<div class="card-body">
					<p>Clave: {{$inmueble->key}}</p>
					<p>Dirección: {{$inmueble->ubicacion->calle}} #{{$inmueble->ubicacion->numero}}</p>
				</div>
	      <img class="card-img-top" src="{{$inmueble->ubicacion->imagenes->first()->url}}" alt="Card image cap">
	      <div class="card-body">
	        <div class="row">
	            <p class="card-text col-md-4 text-secondary"><a style="color:gray" href="{{ url("/inmuebles/{$inmueble->id}") }}"><i class="fas fa-eye fa-4x "></i></a>
	            @auth
	            <p class="card-text col-md-4 text-secondary"><a style="color:gray" class="text-second" href="{{ url("/inmuebles/{$inmueble->id}/edit") }}"><i class="fas fa-pen fa-4x "> </i></a>
	            <form method="POST" action="{{ route('inmuebles.destroy',$inmueble->id) }}"  role="form" class="col-md-4 text-secondary">
	                   {{csrf_field()}}
	                   <input name="_method" type="hidden" value="DELETE">
	                   <button class="btn btn-primary-outline" style="color:gray" type="submit"><i class="fas fa-trash-alt fa-4x"></i></button>
	            </form>
	            @endauth
	        </div>
	      </div>
	    </div>
		</div>

    @endforeach
</div>
@endsection
@section('scripts')
    @parent
		<script type="text/javascript">

		function initialize() {

				const latitude = 19.3545705;
				const longitude = -99.084642;
				 var data = {!! json_encode($inmuebles, JSON_HEX_TAG) !!};
				 console.log(data.length)

				const map = new google.maps.Map(document.getElementById('address-map'), {
						center: {lat: latitude, lng: longitude},
						zoom: 13
				});
				for (var i = 0; i < data.length; i++) {
					console.log(data[i].ubicacion.lat)
					console.log(data[i].ubicacion.lng)

					const marker = new google.maps.Marker({
							map: map,
							position: {lat: data[i].ubicacion.lng, lng: data[i].ubicacion.lat},
					});

					marker.setVisible(true);
				}

			}
		</script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>

@stop
